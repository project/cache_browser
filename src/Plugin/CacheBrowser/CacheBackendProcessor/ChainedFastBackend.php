<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBackendProcessor;

use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface;

/**
 * Provides a cache-backend processor for the BackendChain processor.
 *
 * @CacheBackendProcessor(
 *   id = "chained_fast_backend",
 *   admin_label = @Translation("Chained fast Backend"),
 *   applies_to = {
 *     "Drupal\Core\Cache\ChainedFastBackend"
 *   },
 *   browseable = false
 * )
 */
class ChainedFastBackend extends PluginBase implements PluginInterface {

  /**
   * {@inheritdoc}
   */
  public function count() : ?int {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function size() : ?int {
    return NULL;
  }

}
