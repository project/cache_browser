<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBackendProcessor;

use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\TableSortExtender;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a cache-backend processor for the DatabaseBackend cache.
 *
 * @CacheBackendProcessor(
 *   id = "database_backend",
 *   admin_label = @Translation("Database backend"),
 *   applies_to = {
 *     "Drupal\Core\Cache\DatabaseBackend"
 *   }
 * )
 */
class DatabaseBackend extends PluginBase implements PluginInterface, ContainerFactoryPluginInterface {

  /**
   * SQL query to determine the size of a database table.
   *
   * @var string
   */
  const SIZE_QUERY = 'SELECT DATA_LENGTH FROM information_schema.TABLES WHERE TABLE_NAME = :table AND TABLE_SCHEMA = :database';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The name of the database.
   *
   * @var string
   */
  protected $databaseName;

  /**
   * The name of the database table which contains this cache bin.
   *
   * @var string
   */
  protected $tableName;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->databaseName = $this->database->getConnectionOptions()['database'];

    // Use reflection to get the table name from the bin definition.
    // Note that the 'cache_' prefix is already stored in the bin definition.
    $this->tableName = $this->getBinProperty('bin');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function count() : ?int {
    $query = $this
      ->database
      ->select($this->tableName);

    return $query
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function size() : ?int {
    return $this
      ->database
      ->query(self::SIZE_QUERY, [
        ':table' => $this->tableName,
        ':database' => $this->databaseName,
      ])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function getCids() : array {
    $query = $this
      ->database
      ->select($this->tableName)
      ->fields($this->tableName, ['cid']);

    return $query
      ->execute()
      ->fetchcol();
  }

  /**
   * {@inheritdoc}
   */
  public function getData($header = []) : array {
    $query = $this
      ->database
      ->select($this->tableName)
      ->fields($this->tableName)
      ->extend(PagerSelectExtender::class)->limit(25)
      ->extend(TableSortExtender::class)->orderByHeader($header);

    return $query
      ->execute()
      ->fetchAllAssoc('cid', \PDO::FETCH_ASSOC);
  }

}
