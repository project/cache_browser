<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBackendProcessor;

use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\TableSort;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a cache processor for the ApcuBackend cache.
 *
 * @CacheBackendProcessor(
 *   id = "apcu_backend",
 *   admin_label = @Translation("APCU backend"),
 *   applies_to = {
 *     "Drupal\Core\Cache\ApcuBackend"
 *   }
 * )
 */
class ApcuBackend extends PluginBase implements PluginInterface, ContainerFactoryPluginInterface {

  /**
   * The request object, to provide context for table sorting.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
   return new static(
     $configuration,
     $plugin_id,
     $plugin_definition,
     $container->get('request_stack')->getCurrentRequest()
   );
  }

  /**
   * {@inheritdoc}
   */
  public function count() : ?int {
    return $this->getApcuIterator()->getTotalCount();
  }

  /**
   * {@inheritdoc}
   */
  public function size() : ?int {
    return $this->getApcuIterator()->getTotalSize();
  }

  /**
   * {@inheritdoc}
   */
  public function getCids() : array {
    $cids = [];
    foreach ($this->getApcuIterator() as $key => $value) {
      $cids[] = $key;
    }
    return $cids;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($header = []) : array {
    $iterator = $this->getApcuIterator();

    // @todo Apply sorting, paging, and filtering directly to the iterator.
    // $context = TableSort::getContextFromRequest($header, $this->request);
    // $sortBy = $context['sql'];
    // $sortOrder = $context['sort'];

    $rows = [];
    foreach ($iterator as $key => $value) {
      $data = [
        'cid' => $value['value']->cid,
        'data' => $value['value']->data,
        'expire' => $value['value']->expire,
        'created' => $value['value']->created,
      ];
      // $data = array_intersect_key((array) $value['value'], $header);
      // $data = (array) $value['value'];
      if (!is_string($data['data'])) {
        $data['data'] = serialize($data['data']);
      }
      $rows[] = $data;
    }
    return $rows;
  }

  /**
   * Get the APCU Iterator.
   *
   * @return \APCUIterator
   *   The APCU iterator.
   */
  private function getApcuIterator() : \APCUIterator {
    $reflectionClass = new \ReflectionClass($this->bin());
    $reflectionMethod = $reflectionClass->getMethod('getAll');
    return $reflectionMethod->invoke($this->bin());
  }

}
