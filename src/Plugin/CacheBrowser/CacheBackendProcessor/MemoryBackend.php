<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBackendProcessor;

use Drupal\cache_browser\AccessProtectedPropertyTrait;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface;

/**
 * Provides a cache processor for the MemoryBackend cache.
 *
 * @CacheBackendProcessor(
 *   id = "memory_backend",
 *   admin_label = @Translation("Memory backend"),
 *   applies_to = {
 *     "Drupal\Core\Cache\MemoryBackend"
 *   },
 *   browseable = false
 * )
 */
class MemoryBackend extends PluginBase implements PluginInterface {

  use AccessProtectedPropertyTrait;

  /**
   * {@inheritdoc}
   */
  public function count() : ?int {
    return count($this->getCache());
  }

  /**
   * {@inheritdoc}
   */
  public function size() : ?int {
    return NULL;
  }

  /**
   * Use reflection to get the cache data from the bin.
   *
   * @return array
   *   The cached data in the cache bin.
   */
  private function getCache() : array {
    return $this->getBinProperty('cache');
  }

}
