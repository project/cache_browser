<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBackendProcessor;

use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface;

/**
 * Provides a cache-backend processor for the BackendChain processor.
 *
 * @CacheBackendProcessor(
 *   id = "backend_chain",
 *   admin_label = @Translation("Backend chain"),
 *   applies_to = {
 *     "Drupal\Core\Cache\BackendChain"
 *   },
 *   browseable = false
 * )
 */
class BackendChain extends PluginBase implements PluginInterface {

  /**
   * {@inheritdoc}
   */
  public function count() : ?int {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function size() : ?int {
    return NULL;
  }

}
