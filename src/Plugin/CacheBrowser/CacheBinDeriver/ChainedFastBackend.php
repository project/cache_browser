<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBinDeriver;

use Drupal\cache_browser\AccessProtectedPropertyTrait;
use Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginInterface;

/**
 * Derive child bins for BackendChain cache bin.
 *
 * @CacheBinDeriver(
 *   id = "chained_fast_backend",
 *   admin_label = @Translation("Chained fast backend"),
 *   applies_to = {
 *     "Drupal\Core\Cache\ChainedFastBackend"
 *   }
 * )
 */
class ChainedFastBackend extends PluginBase implements PluginInterface {

  use AccessProtectedPropertyTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeBins() : array {
    return [
      'fastBackend' => $this->getProtectedProperty($this->configuration['bin'], 'fastBackend'),
      'consistentBackend' => $this->getProtectedProperty($this->configuration['bin'], 'consistentBackend'),
    ];
  }

}
