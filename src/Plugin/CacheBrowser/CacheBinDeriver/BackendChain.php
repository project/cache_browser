<?php

namespace Drupal\cache_browser\Plugin\CacheBrowser\CacheBinDeriver;

use Drupal\cache_browser\AccessProtectedPropertyTrait;
use Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginBase;
use Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginInterface;

/**
 * Derive child bins for BackendChain cache bin.
 *
 * @CacheBinDeriver(
 *   id = "backend_chain",
 *   admin_label = @Translation("Backend chain"),
 *   applies_to = {
 *     "Drupal\Core\Cache\BackendChain"
 *   }
 * )
 */
class BackendChain extends PluginBase implements PluginInterface {

  use AccessProtectedPropertyTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeBins() : array {
    return $this->getProtectedProperty($this->configuration['bin'], 'backends');
  }

}
