<?php

namespace Drupal\cache_browser\PluginDefinition\CacheBinDeriver;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface to be implemented by all cache bin deriver plugins.
 */
interface PluginInterface extends PluginInspectionInterface {

  /**
   * Get the derivative child bins.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface[]
   *   Array of derivative child bins, with an index appropriate to the source
   *   bin.
   */
  public function getDerivativeBins() : array;

}
