<?php

namespace Drupal\cache_browser\PluginDefinition\CacheBinDeriver;

use Drupal\Component\Plugin\PluginBase as PluginPluginBase;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Base class for defining cache bin deriver plugins.
 */
abstract class PluginBase extends PluginPluginBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if (empty($this->configuration['bin']) || !($this->configuration['bin'] instanceof CacheBackendInterface)) {
      throw new \Exception('A cache bin is a required configuration parameter.');
    }
  }

}
