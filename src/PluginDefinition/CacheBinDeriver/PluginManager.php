<?php

namespace Drupal\cache_browser\PluginDefinition\CacheBinDeriver;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for cache-backend processor plugins.
 *
 * @see \Drupal\cache_browser\Annotation\CacheBinDeriver
 * @see \Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager {

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/CacheBrowser/CacheBinDeriver',
      $namespaces,
      $module_handler,
      'Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginInterface',
      'Drupal\cache_browser\Annotation\CacheBinDeriver'
    );
    $this->alterInfo('cache_browser_bin_deriver_info');
    $this->setCacheBackend($cache_backend, 'cache_browser_bin_deriver_info_plugins');
  }

  /**
   * Get the derivatives for a particular cache bin.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $bin
   *   A cache bin.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface[]
   *   Array of derivatives. An empty array is returned if no derivatives
   *   exist. The array keys will be relevant to the type of bin.
   */
  public function getDerivativeBins(CacheBackendInterface $bin) : ?array {
    if ($pluginId = $this->getPluginFor($bin)) {
      /** @var \Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginInterface */
      $plugin = $this->createInstance($pluginId, [
        'bin' => $bin,
      ]);

      return $plugin->getDerivativeBins();
    }
    return NULL;
  }

  /**
   * Get the deriver plugin for a particular bin class.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $bin
   *   A cache bin.
   *
   * @return string
   *   The ID of a deriver plugin, if a deriver exists.
   */
  protected function getPluginFor(CacheBackendInterface $bin) : ?string {
    foreach ($this->getPluginClassMap() as $class => $pluginId) {
      if ($bin instanceof $class) {
        return $pluginId;
      }
    }
    return NULL;
  }

  /**
   * Map the classes supported by each plugin.
   *
   * @return array
   *   The array key of each entry is the classname of a class which implements
   *   the CacheBackendInterface.
   *   The value is the ID of the plugin which can process bins of that type.
   */
  protected function getPluginClassMap() : array {
    static $map = [];
    if (empty($map)) {
      foreach ($this->getDefinitions() as $id => $definition) {
        if (!empty($definition['applies_to'])) {
          foreach ($definition['applies_to'] as $className) {
            $map[$className] = $id;
          }
        }
      }
    }
    return $map;
  }

}
