<?php

namespace Drupal\cache_browser\PluginDefinition\CacheBackendProcessor;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Interface to be implemented by all cache-browser processor plugins.
 */
interface PluginInterface extends PluginInspectionInterface {

  /**
   * The number of items stored in the cache.
   *
   * @return int
   *   The number of items stored in the cache.
   */
  public function count() : ?int;

  /**
   * The size of the cached data, in bytes.
   *
   * @return int
   *   The size of the cached data, in bytes.
   */
  public function size() : ?int;

  /**
   * The cache IDs of the items stored in the cache.
   *
   * @return array
   *   The cache IDs of the items stored in the cache.
   */
  public function getCids() : array;

  /**
   * The data stored in the cache.
   *
   * @param array $header
   *   The table header, in order to provider sorting and pagination.
   *
   * @return array
   *   The data stored in the cache.
   */
  public function getData($header = []) : array;

  /**
   * The label / name for this plugin processor.
   *
   * @return string
   *   The label / name for this plugin processor.
   */
  public function label();

  /**
   * Get the cache bin which is managed by this processor.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The cache bin which is managed by this processor.
   */
  public function bin() : CacheBackendInterface;

  /**
   * Determine whether the processor has implemented a cache browser.
   *
   * @return bool
   *   TRUE if there is a cache-browser for this type of processor.
   */
  public function isBrowseable() : bool;

}
