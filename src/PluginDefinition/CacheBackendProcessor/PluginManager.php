<?php

namespace Drupal\cache_browser\PluginDefinition\CacheBackendProcessor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for cache-backend processor plugins.
 *
 * @see \Drupal\cache_browser\Annotation\CacheBackendProcessor
 * @see \Drupal\cache_browser\PluginDefinition\CacheBackendProcessorPluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager {

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/CacheBrowser/CacheBackendProcessor',
      $namespaces,
      $module_handler,
      'Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface',
      'Drupal\cache_browser\Annotation\CacheBackendProcessor'
    );
    $this->alterInfo('cache_browser_cache_backend_processor_info');
    $this->setCacheBackend($cache_backend, 'cache_browser_cache_backend_processor_info_plugins');
  }

  /**
   * Identify which processor plugin should be used for a particular bin.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $bin
   *   The cache bin to identify a processor for.
   *
   * @return ?string
   *   A plugin ID, if the bin is supported.
   */
  public function getProcessorPluginIdFor(CacheBackendInterface $bin) : ?string {
    foreach ($this->getPluginClassMap() as $class => $plugin_id) {
      if ($bin instanceof $class) {
        return $plugin_id;
      }
    }
    return NULL;
  }

  /**
   * Determine whether a plugin can derive child bins.
   *
   * @param string $plugin_id
   *   The ID of a CacheBackendProcessor plugin.
   *
   * @return bool
   *   TRUE if the processor can derive child bins.
   */
  public function isBinDeriver($plugin_id) {
    $class = $this->getDefinition($plugin_id)['class'];
    return is_a($class, '\Drupal\cache_browser\PluginDefinition\CacheBinDeriverInterface', TRUE);
  }

  /**
   * Get the plugin processor for a specific cache bin.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $bin
   *   The cache bin to identify a processor for.
   * @param string $id
   *   (optional) The cache bin ID.
   *
   * @return \Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface
   *   The plugin. If there isn't a plugin for this particular bin-type, NULL
   *   is returned.
   */
  public function getPluginFor(CacheBackendInterface $bin, string $id = NULL) : ?PluginInterface {
    $map = $this->getPluginClassMap();
    foreach ($map as $className => $pluginId) {
      if ($bin instanceof $className) {
        $config = [
          'bin' => $bin,
          'bin_name' => $id,
        ];

        /** @var \Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface */
        $plugin = $this->createInstance($pluginId, $config);
        return $plugin;
      }
    }
    return NULL;
  }

  /**
   * Map the classes supported by each plugin.
   *
   * @return array
   *   The array key of each entry is the classname of a class which implements
   *   the CacheBackendInterface.
   *   The value is the ID of the plugin which can process bins of that type.
   */
  protected function getPluginClassMap() : array {
    static $map = [];
    if (empty($map)) {
      foreach ($this->getDefinitions() as $id => $definition) {
        if (!empty($definition['applies_to'])) {
          foreach ($definition['applies_to'] as $className) {
            $map[$className] = $id;
          }
        }
      }
    }
    return $map;
  }

}
