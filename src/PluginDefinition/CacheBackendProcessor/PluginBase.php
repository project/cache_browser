<?php

namespace Drupal\cache_browser\PluginDefinition\CacheBackendProcessor;

use Drupal\cache_browser\AccessProtectedPropertyTrait;
use Drupal\Component\Plugin\PluginBase as PluginPluginBase;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Base class for defining cache-backend processor plugins.
 */
class PluginBase extends PluginPluginBase {

  use AccessProtectedPropertyTrait;

  /**
   * {@inheritdoc}
   */
  public function getCids() : array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getData($header = []) : array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (!empty($this->pluginDefinition['admin_label'])) {
      return $this->pluginDefinition['admin_label'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function bin() : CacheBackendInterface {
    if (!array_key_exists('bin', $this->configuration) || !($this->configuration['bin'] instanceof CacheBackendInterface)) {
      throw new \Exception('A cache bin must be provided.');
    }
    return $this->configuration['bin'];
  }

  /**
   * {@inheritdoc}
   */
  public function binName() : ?string {
    $inferredName = $this->inferBinName();
    if (array_key_exists('bin_name', $this->configuration) && $this->configuration['bin_name'] !== '') {
      if ($this->configuration['bin_name'] === $inferredName || empty($inferredName)) {
        return $this->configuration['bin_name'];
      }
      return sprintf('%s (%s)', $this->configuration['bin_name'], $inferredName);
    }
    return $inferredName;
  }

  /**
   * {@inheritdoc}
   */
  public function isBrowseable() : bool {
    $browseable = $this->pluginDefinition['browseable'] ?? TRUE;
    return $browseable;
  }

  /**
   * Attempt to infer the bin name from the bin definition.
   *
   * @return string
   *   The name of the bin, if it can be inferred.
   */
  protected function inferBinName() : ?string {
    $bin = $this->getBinProperty('bin');
    if ($bin && is_string($bin)) {
      if (strpos($bin, 'cache_') === 0) {
        return substr($bin, 6);
      }
      return $bin;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getBinProperty(string $property) {
    return $this->getProtectedProperty($this->bin(), $property);
  }

}
