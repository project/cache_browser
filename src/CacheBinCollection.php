<?php

namespace Drupal\cache_browser;

use Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginManager;

/**
 * An iterable collection of cache bins.
 */
class CacheBinCollection extends \RecursiveArrayIterator {

  /**
   * The plugin manager for cache-bin deriver plugins.
   *
   * @var \Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginManager
   */
  protected $pluginManager;

  /**
   * Set the plugin manager for cache-bin deriver plugins.
   *
   * @param \Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginManager $plugin_manager
   *   The plugin manager for cache-bin deriver plugins.
   */
  public function setPluginManager(PluginManager $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren() : ?self {
    $bin = $this->current();
    $derivatives = $this->pluginManager->getDerivativeBins($bin);
    if (!empty($derivatives)) {
      $collection = new CacheBinCollection($derivatives);
      $collection->setPluginManager($this->pluginManager);
      $collection->ksort();
      return $collection;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasChildren() : bool {
    $bin = $this->current();
    $derivatives = $this->pluginManager->getDerivativeBins($bin);
    return (!empty($derivatives));
  }

  /**
   * Seek, using an associative-array key.
   */
  public function seekKey($key) : void {
    $keys = array_keys($this->getArrayCopy());
    $pos = array_search($key, $keys);
    $this->seek($pos);
  }

}
