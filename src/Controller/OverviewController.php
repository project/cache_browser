<?php

namespace Drupal\cache_browser\Controller;

use Drupal\cache_browser\CacheBinCollection;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the Cache Browser.
 */
class OverviewController extends ControllerBase {

  /**
   * Plugin manager for cache-browser processor plugins.
   *
   * @var \Drupal\cache_browser\PluginDefinition\CacheBackendProcessorPluginManager
   */
  protected $pluginManager;

  /**
   * Cache-bin collection.
   *
   * @var \Drupal\cache_browser\CacheBinCollection
   */
  protected $cacheBinCollection;

  /**
   * Constructor.
   *
   * @param \Drupal\cache_browser\CacheBinCollection $cache_bin_collection
   *   The cache-bin collection.
   * @param \Drupal\cache_browser\PluginDefinition\CacheBackendProcessorPluginManager $plugin_manager
   *   Plugin manager for cache-browser processor plugins.
   */
  public function __construct(CacheBinCollection $cache_bin_collection, PluginManager $plugin_manager) {
    $this->cacheBinCollection = $cache_bin_collection;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache_browser.cache_bin_collection'),
      $container->get('plugin.manager.cache_backend_processor')
    );
  }

  /**
   * Page controller for the overview summary page which lists the cache bins.
   */
  public function summary() {
    $build = [];

    $build['#cache'] = [
      'max-age' => 0,
    ];

    $build['cache_bins'] = [
      '#type' => 'table',
      '#caption' => $this->t('Cache bins'),
      '#header' => $this->getHeader(),
      '#rows' => $this->getRows($this->cacheBinCollection),
      '#attributes' => [
        'class' => [
          'cache-browser',
          'summary',
        ],
      ],
    ];

    $build['cache_bins']['#attached'] = [
      'library' => [
        'cache_browser/browser',
      ],
    ];

    return $build;
  }

  /**
   * Get the table header for the overview table.
   *
   * @return array
   *   A header to be used with a table display.
   */
  protected function getHeader() {
    return [
      'bin_id'     => $this->t('Bin ID'),
      'count'      => $this->t('Number of cached items'),
      'size'       => $this->t('Size in use'),
      'class'      => $this->t('Cache class'),
      'operations' => $this->t('Operations'),
    ];
  }

  /**
   * Get the rows for a cache-bin collection.
   *
   * @param \Drupal\cache_browser\CacheBinCollection $bins
   *   A cache-bin collection.
   *
   * @return array
   *   Definition of table rows for a cache-bin collection.
   */
  protected function getRows(CacheBinCollection $bins) : array {
    static $prefixes = [];

    $rows = [];
    foreach ($bins as $bin_id => $bin) {
      $row = $this->getRow($bin, $bin_id, $prefixes);
      $rows[] = $row;

      if ($bins->hasChildren()) {
        $prefixes[] = $bin_id;
        $rows = array_merge($rows, $this->getRows($bins->getChildren()));
        array_pop($prefixes);
      }
    }
    return $rows;
  }

  /**
   * Get the row for a single cache bin.
   *
   * @param Drupal\core\Cache\CacheBackendInterface $bin
   *   A cache-bin.
   * @param string $bin_id
   *   The ID of the bin.
   * @param array $prefixes
   *   (optional) Array containing the IDs of any parent bins.
   *
   * @return array
   *   A table row definition.
   */
  protected function getRow(CacheBackendInterface $bin, string $bin_id, $prefixes = []) : array {
    $result = $this->getTemplateRow();

    $fqid = implode(':', array_merge($prefixes, [$bin_id]));
    $id = $fqid;
    $result['bin_id']['data'] = $id;

    $depth = count($prefixes);
    if ($depth) {
      $result['bin_id']['style'] = 'padding-left: ' . (12 + ($depth * 25)) . 'px';
      $result['class']['style']  = 'padding-left: ' . (12 + ($depth * 25)) . 'px';
    }

    $plugin = $this->pluginManager->getPluginFor($bin, (string) $bin_id);
    if ($plugin) {
      if ($plugin->isBrowseable()) {
        $id = Link::createFromRoute(
          $plugin->binName(),
          'cache_browser.browser', [
            'bin' => $fqid,
          ]);
      }

      $result['bin_id']['data'] = $id;
      $result['count']['data'] = ($plugin->count() !== NULL) ? number_format($plugin->count()) : '';
      $result['size']['data'] = ($plugin->size() !== NULL) ? format_size($plugin->size()) : '';
      $result['class']['data'] = $plugin->label();
    }
    $result['operations']['data'] = $this->getOperations($fqid, $plugin);

    $row = [
      'data' => $result,
    ];
    return $row;
  }

  /**
   * Fetch the row definition for an empty row.
   *
   * @return array
   *   An empty table row.
   */
  protected function getTemplateRow() : array {
    $header = array_keys($this->getHeader());
    $result = array_fill_keys($header, ['data' => '']);
    return $result;
  }

  /**
   * Get the operations available for a particaular table row / cache bin.
   *
   * @param string $id
   *   The bin ID.
   * @param \Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginInterface $plugin
   *   (optional) The backend processor plugin, if it's defined for the bin.
   *
   * @return array
   *   An 'Operations' render element.
   */
  protected function getOperations($id, PluginInterface $plugin = NULL) {
    $operations = [
      '#type' => 'operations',
      '#title' => $this->t('Operations'),
      '#links' => [],
    ];

    if ($plugin && $plugin->isBrowseable()) {
      $operations['#links']['browse'] = [
        'title' => $this->t('Browse cache'),
        'url' => Url::fromRoute('cache_browser.browser', ['bin' => $id]),
      ];
    }

    $operations['#links']['clear_cache'] = [
      'title' => $this->t('Clear cache'),
      'url' => Url::fromRoute('cache_browser.clear_cache', ['bin' => $id]),
    ];

    return $operations;
  }

}
