<?php

namespace Drupal\cache_browser\Controller;

use Drupal\cache_browser\Form\CacheCidForm;
use Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the Cache Browser.
 */
class CacheBrowserController extends ControllerBase {

  /**
   * Plugin manager for cache-browser processor plugins.
   *
   * @var \Drupal\cache_browser\PluginDefinition\CacheBackendProcessorPluginManager
   */
  protected $pluginManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The ID of the bin currently being processed.
   *
   * @var string
   */
  protected $bin_id;

  /**
   * Constructor.
   *
   * @param \Drupal\cache_browser\PluginDefinition\CacheBackendProcessor\PluginManager $plugin_manager
   *   The plugin manager for cache-browser processor plugins.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(PluginManager $plugin_manager, DateFormatterInterface $date_formatter) {
    $this->pluginManager = $plugin_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.cache_backend_processor'),
      $container->get('date.formatter'));
  }

  /**
   * Get the table header for rendering the cache data.
   */
  protected function getTableHeader() {
    $header = [
      'cid' => [
        'data' => $this->t('Cache ID'),
        'field' => 'cid',
      ],
      'data' => [
        'data' => $this->t('Data'),
        'field' => 'data',
      ],
      'expire' => [
        'data' => $this->t('Expiry time'),
        'field' => 'expire',
      ],
      'created' => [
        'data' => $this->t('Created'),
        'field' => 'created',
      ],
    ];
    return $header;
  }

  /**
   * Page controller for browsing an individual bin.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $bin
   *   The cache bin.
   *
   * @return array
   *   A renderable array.
   */
  public function browseBin(CacheBackendInterface $bin, RouteMatchInterface $route_match) {
    $this->bin_id = $route_match->getRawParameter('bin');
    $build = [];

    $build['#cache'] = [
      'max-age' => 0,
    ];

    $build['pager_above'] = [
      '#type' => 'pager',
    ];
    $build['cache_bins'] = [
      '#type'       => 'table',
      '#caption'    => $this->t('Cached data'),
      '#header'     => $this->getTableHeader(),
      '#rows'       => [],
      '#attributes' => [
        'class' => [
          'cache-browser',
          'browser',
        ],
      ],
    ];
    $build['cache_bins']['#attached'] = [
      'library' => [
        'cache_browser/browser',
      ],
    ];
    $build['pager_below'] = [
      '#type' => 'pager',
    ];

    $processor = $this->pluginManager->getPluginFor($bin);
    foreach ($processor->getData($this->getTableHeader()) as $data) {
      $row = $this->prepareTableRow($data);
      $build['cache_bins']['#rows'][] = [
        'data' => $row,
      ];
    }

    return $build;
  }

  /**
   * Title callback for browsing an individual bin.
   */
  public function browseBinTitle(RouteMatchInterface $route_match) {
    return $this->t('Browse @bin cache', [
      '@bin' => $route_match->getRawParameter('bin'),
    ]);
  }

  /**
   * Prepare the data for visualisation as a row in a data table.
   */
  public function prepareTableRow($data) {
    $row = array_intersect_key($data, $this->getTableHeader());

    $row['cid'] = Link::createFromRoute(
      $data['cid'],
      'cache_browser.browser.cid', [
        'bin' => $this->bin_id,
        'cid' => CacheCidForm::encodeCid($data['cid']),
      ]);

    $row['data'] = [
      'data' => [
        '#plain_text' => $data['data'],
        '#prefix' => '<pre>',
        '#suffix' => '</pre>',
      ],
    ];

    if ($row['expire'] == CacheBackendInterface::CACHE_PERMANENT) {
      $row['expire'] = $this->t('Permanent');
    }
    else {
      $row['expire'] = $this->dateFormatter->formatTimeDiffUntil($row['expire']);
    }
    $row['created'] = $this->dateFormatter->format((int) $row['created']);

    return $row;
  }

}
