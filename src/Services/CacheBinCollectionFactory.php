<?php

namespace Drupal\cache_browser\Services;

use Drupal\cache_browser\CacheBinCollection;
use Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Factory service to create a cache-bin collection.
 */
class CacheBinCollectionFactory implements ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * Plugin manager for cache bin deriver plugins.
   *
   * @var \Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginManager
   */
  protected $manager;

  /**
   * The '%cache_bins%' parameter defined in the DI container.
   *
   * @var array
   */
  protected $cacheBinsContainerDefinition;

  /**
   * Collection of cache bins.
   *
   * @var \Drupal\cache_browser\CacheBinCollection
   */
  protected $cacheBinCollection;

  /**
   * Constructor.
   *
   * @param \Drupal\cache_browser\PluginDefinition\CacheBinDeriver\PluginManager $manager
   *   The plugin manager for cache bin deriver plugins.
   */
  public function __construct(PluginManager $manager) {
    $this->manager = $manager;
  }

  /**
   * Set the value of the '%cache_bins%' parameter defined in the DI container.
   *
   * @param array $cache_bins
   *   The value of the '%cache_bins%' parameter defined in the DI container.
   */
  public function setCacheBins(array $cache_bins) {
    $this->cacheBinsContainerDefinition = $cache_bins;
  }

  /**
   * Factory service to create a cache-bin collection.
   *
   * @return \Drupal\cache_browser\CacheBinCollection
   *   A cache-bin collection is a recursive iterator for cache bins.
   */
  public function createCollection() : CacheBinCollection {
    if (!$this->cacheBinCollection) {
      $this->doCreateCollection();
    }
    return $this->cacheBinCollection;
  }

  /**
   * Create the cache-bin collection.
   */
  protected function doCreateCollection() {
    $collection = new CacheBinCollection();
    $collection->setPluginManager($this->manager);

    foreach ($this->cacheBinsContainerDefinition as $service_id => $bin_name) {
      $bin = $this->container->get($service_id);
      $collection[$bin_name] = $bin;
    }

    $collection->ksort();

    $this->cacheBinCollection = $collection;
  }

}
