<?php

namespace Drupal\cache_browser\Routing;

use Drupal\cache_browser\CacheBinCollection;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Route parameter upcaster for cache bins.
 *
 * This supports descendant bins using ':' as a separator.
 */
class CacheBinConverter implements ParamConverterInterface {

  /**
   * The cache-bin collection.
   *
   * @var \Drupal\cache_browser\CacheBinCollection
   */
  protected $cacheBinCollection;

  /**
   * Constructor.
   *
   * @param \Drupal\cache_browser\CacheBinCollection $cache_bin_collection
   *   The cache-bin collection.
   */
  public function __construct(CacheBinCollection $cache_bin_collection) {
    $this->cacheBinCollection = $cache_bin_collection;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $bins = $this->cacheBinCollection;
    if (!empty($value)) {
      $parts = explode(':', $value);

      do {
        $part = array_shift($parts);
        $bins->seekKey($part);
        if ($parts) {
          $bins = $bins->getChildren();
        }
      } while ($parts);

      return $bins->current();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return !empty($definition['type']) && $definition['type'] == 'cache-bin';
  }

}
