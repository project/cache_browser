<?php

namespace Drupal\cache_browser\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * A processor to analyze a cache backend.
 *
 * @Annotation
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class CacheBackendProcessor extends Plugin {

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  public $id;

}
