<?php

namespace Drupal\cache_browser\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * A plugin which fetches child bins from caches with derivative bins.
 *
 * @Annotation
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class CacheBinDeriver extends Plugin {

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  public $id;

}
