<?php

namespace Drupal\cache_browser;

/**
 * Allow access to protected object properties.
 */
trait AccessProtectedPropertyTrait {

  /**
   * Fetch the protected property of an object.
   *
   * @param object $object
   *   An instantiated PHP class.
   * @param string $property
   *   The name of the property to access.
   *
   * @return mixed
   *   The value of the property, if it exists, or NULL.
   */
  protected function getProtectedProperty($object, $property) {
    $reflectionClass = new \ReflectionClass($object);
    if (!$reflectionClass->hasProperty($property)) {
      return NULL;
    }
    $reflectionProperty = $reflectionClass->getProperty($property);
    return $reflectionProperty->getValue($object);
  }

}
