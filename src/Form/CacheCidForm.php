<?php

namespace Drupal\cache_browser\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to view, and optionally edit, a cache entry.
 */
class CacheCidForm extends FormBase {

  /**
   * Number of rows to use when displaying the text areas for the cache values.
   *
   * @var int
   */
  const CACHE_VALUE_TEXTAREA_ROWS = 30;

  /**
   * The cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $bin;

  /**
   * The bin ID.
   *
   * @var string
   */
  protected $bin_id;

  /**
   * The cache ID.
   *
   * @var string
   */
  protected $cid;

  /**
   * The cache entry.
   *
   * @var stdclass
   */
  protected $entry;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cache_browser.edit_cid';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CacheBackendInterface $bin = NULL, string $cid  = '') {
    $this->bin = $bin;

    $cid = self::decodeCid($cid);
    $entry = $bin->get($cid, TRUE);
    if (!$entry) {
      throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
    }

    $data = [
      'var_export' => var_export($entry->data, TRUE),
      'json' => json_encode($entry->data, JSON_PRETTY_PRINT),
      'serialized' => serialize($entry->data),
    ];

    $form['#cache'] = [
      'max-age' => 0,
    ];

    $form['cid'] = [
      '#type' => 'textarea',
      '#title' => $this->t('CID'),
      '#value' => $cid,
      '#read_only' =>  TRUE,
    ];

    $form['data'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Cache value'),
    ];
    $form['data']['var_export'] = [
      '#type' => 'details',
      '#title' => $this->t('PHP var_export'),
      '#group' => 'data',
    ];
    $form['data']['var_export']['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PHP var_export'),
      '#value' => $data['var_export'],
      '#read_only' =>  TRUE,
      '#rows' => self::CACHE_VALUE_TEXTAREA_ROWS,
    ];
    $form['data']['serialized'] = [
      '#type' => 'details',
      '#title' => $this->t('PHP serialized'),
      '#group' => 'data',
    ];
    $form['data']['serialized']['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('PHP serialized'),
      '#value' => $data['serialized'],
      '#read_only' =>  TRUE,
      '#rows' => self::CACHE_VALUE_TEXTAREA_ROWS,
    ];
    $form['data']['json'] = [
      '#type' => 'details',
      '#title' => $this->t('JSON'),
      '#group' => 'data',
    ];
    $form['data']['json']['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('JSON'),
      '#value' => $data['json'],
      '#read_only' =>  TRUE,
      '#rows' => self::CACHE_VALUE_TEXTAREA_ROWS,
    ];

    $form['metadata'] = [
      '#type' => 'details',
      '#title' => $this->t('Metadata'),
      '#open' => TRUE,
    ];
    $form['metadata']['expire'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expiry'),
      '#value' => $entry->expire,
      '#description' => ($entry->expire == CacheBackendInterface::CACHE_PERMANENT)
        ? $this->t('Permanent')
        : $this->dateFormatter->formatTimeDiffUntil($entry->expire),
      '#description_display' => 'after',
      '#read_only' => TRUE,
    ];
    $form['metadata']['created'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Created'),
      '#value' => $entry->created,
      '#description' => $this->dateFormatter->format((int) $entry->created),
      '#description_display' => 'after',
      '#read_only' => TRUE,
    ];
    $form['metadata']['serialized'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Serialized'),
      '#value' => $entry->serialized ? $this->t('Yes') : $this->t('No'),
      '#read_only' => TRUE,
    ];
    $form['metadata']['tags'] = [
      '#type' => 'details',
      '#title' => $this->t('Tags'),
      '#open' => TRUE,
    ];
    foreach ($entry->tags as $tag) {
      $form['metadata']['tags'][] = [
        '#type' => 'textfield',
        '#value' => $tag,
        '#read_only' => TRUE,
      ];
    };

    if (property_exists($entry, 'context')) {
      $form['metadata']['contexts'] = [
        '#type' => 'details',
        '#title' => $this->t('Contexts'),
        '#open' => TRUE,
      ];
      foreach ($entry->contexts as $context) {
        $form['metadata']['contextss'][] = [
          '#type' => 'textfield',
          '#value' => $context,
          '#read_only' => TRUE,
        ];
      };
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // No-op: this form is used for visualisation and does not have a submit
    // button.
  }

  /**
   * Encode a cache ID for use as a URL parameter.
   *
   * @param string $cid
   *   The cache ID to encode.
   *
   * @return string
   *   The encoded CID, for use as a path parameter.
   */
  public static function encodeCid(string $cid) {
    return base64_encode($cid);
  }

  /**
   * Decode a URL path parameter to its original CID value.
   *
   * @param string $cid
   *   The encoded CID, used as a path parameter.

   * @return string
   *   The original cache ID value.
   */
  public static function decodeCid(string $cid) {
    return base64_decode($cid);
  }

}
