<?php

namespace Drupal\cache_browser\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Confirmation form for clearing a cache bin.
 */
class CacheClearConfirmForm extends ConfirmFormBase {

  /**
   * Cache bin to evict.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $bin;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CacheBackendInterface $bin = NULL) {
    $this->bin = $bin;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cache_browser.clear_cache';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('cache_browser.summary');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Clear cache?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Are you sure you wish to clear the cache bin? This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Clear cache bin');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Go back to cache browser');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->bin->deleteAll();
    $this->messenger()->addMessage('The cache bin has been cleared.');
    $form_state->setRedirect('cache_browser.summary');
  }

}
